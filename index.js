import express from "express";
import cors from "cors";

import { sendInDiscord } from "./utils/discord/index.js";

const app = express();

app.use(cors());

app.use(
  express.json({
    extended: true,
  })
);

function start() {
  app.listen(5000, () => {
    console.log("Server started");
  });
}

app.post("/webhook", async (req, res) => {
  const response = req.body;
  const respBody = {
    full_name: response.repository.full_name,
    author: response.head_commit.author.name,
    message: response.head_commit.message,
    timestamp: response.head_commit.timestamp,
  };
  let body = {
    embeds: [
      {
        title: "System Info",
        color: "14177041",
        fields: [
          {
            name: `GitHub commit, репозиторий ${respBody.full_name}`,
            value: `Пользователь ${respBody.author} совершил коммит с текстом:\n"${respBody.message}"`,
          },
          {
            name: "Дата коммита",
            value: `${respBody.timestamp}`,
          },
        ],
        timestamp: new Date(Date.now()),
      },
    ],
  };
  await sendInDiscord("932118953407438868", body);

  console.log(respBody);
  res.json(respBody);
});

start();
